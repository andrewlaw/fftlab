/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ed.roslin.fftlab;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;

/**
 *
 * @author Andy Law <andy.law@roslin.ed.ac.uk>
 */
class ComplexSamplesPanel extends Panel {

    public ComplexSamplesPanel(SamplesView realView,
            SamplesView imagView,
            String label) {
        setLayout(new BorderLayout());
        add("North", new Label(label, Label.CENTER));
        Panel panel = new Panel();
        panel.setLayout(new GridLayout(1, 2, 1, 1));
        panel.add(new SamplesPanel(realView, "Real"));
        panel.add(new SamplesPanel(imagView, "Imaginary"));
        add("Center", panel);
    }

    public void paint(Graphics g) {
        Dimension d = size();
        g.setColor(Color.blue);
        g.draw3DRect(0, 0, d.width - 1, d.height - 1, true);
    }

    public Insets insets() {
        return new Insets(1, 1, 1, 1);
    }
}
