/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ed.roslin.fftlab;

import java.awt.BorderLayout;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.WindowEvent;

/**
 *
 * @author Andy Law <andy.law@roslin.ed.ac.uk>
 */
public class FftLab extends Frame {

    MainPanel mainPanel;
    ControlPanel controlPanel;

    public FftLab() {
        FftLabController controller = new FftLabController();
        setLayout(new BorderLayout());
        mainPanel = new MainPanel(controller.fRealView,
                controller.fImagView,
                controller.gRealView,
                controller.gImagView);
        add("Center", mainPanel);
        controlPanel = new ControlPanel(controller);
        add("South", controlPanel);
    }

    public boolean handleEvent(Event e) {
        if (e.id == Event.WINDOW_DESTROY) {
            System.exit(0);
        }
        return false;
    }

    public void windowClosing(WindowEvent e) {
        dispose();
        System.exit(0);
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public static void main(String[] args) {
        FftLab fftlab = new FftLab();
        fftlab.setTitle("FFT Laboratory");
        fftlab.setSize(600, 400);
        fftlab.setVisible(true);
    }
    
}
