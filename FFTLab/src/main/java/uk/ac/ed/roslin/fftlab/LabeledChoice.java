/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ed.roslin.fftlab;

import java.awt.Choice;
import java.awt.Label;
import java.awt.Panel;

/**
 *
 * @author Andy Law <andy.law@roslin.ed.ac.uk>
 */
class LabeledChoice extends Panel {

    public Choice choice;

    public LabeledChoice(String label) {
        add(new Label(label, Label.RIGHT));
        choice = new Choice();
        add(choice);
    }
}
