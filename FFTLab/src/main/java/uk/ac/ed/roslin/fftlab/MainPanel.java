/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ed.roslin.fftlab;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Panel;

/**
 *
 * @author Andy Law <andy.law@roslin.ed.ac.uk>
 */
class MainPanel extends Panel {

    public MainPanel(SamplesView fRealView, SamplesView fImagView,
            SamplesView gRealView, SamplesView gImagView) {
        setLayout(new GridLayout(2, 1, 1, 1));
        add(new ComplexSamplesPanel(fRealView,
                fImagView,
                "f(x)"));
        add(new ComplexSamplesPanel(gRealView,
                gImagView,
                "F(k)"));
    }

    public void paint(Graphics g) {
        Dimension d = size();
        g.setColor(Color.blue);
        g.draw3DRect(0, 0, d.width - 1, d.height - 1, true);
    }

    public Insets insets() {
        return new Insets(1, 1, 1, 1);
    }
}
